<?php
//technical debt: this needs to be split into separate controller and view

require_once("../models/AuditLog.php");
require_once("../models/Employee.php");

//make sure we handle any form processing before attempting to render a UI
if ($_POST) {
    try {
        /**
         * technical debt: this sequence is very similar to 
         * /api/new.php and needs to be moved into a reusable 
         * service/domain layer
         */
        $employee              = new \models\Employee();
        $employee->name        = $_POST['name'];
        $employee->email       = $_POST['email'];
        $employee->phoneNumber = $_POST['phone_number'];
        $employee->type        = $_POST['employee_type'];
        $employee->gender      = $_POST['gender'];
        $employee->password    = sha1($_POST['password']);

        $employee->save();    

        $audit_log          = new \models\AuditLog();
        $timestamp          = date("d/m/y h:i:s");
        $audit_log->message = sprintf("%s was added on %s", $_POST['name'], $timestamp);
        $audit_log->save();

        //notify the new employee
        //technical debt: do we really want plaintext password in this email?
        $email = array(
            'recipient' => $employee->email,
            'subject'   => "Thanks for registering",
            'content'   => sprintf("Dear %s,\nYou have been added to AwesomeCorp! your password is %s.\nYou can login at: %s.\nRegards,\nRecruiterForce",
                                $employee->name, 
                                $_POST['password'], 
                                "http://awesomecorp.recruiterforce.com/login"
                            )
        );
        //technical debt: we should move this to something more accessible like mailchimp
        //technical debt: mailing and updating the flag need to be combined  
        mail($email['recipient'], $email['subject'], $email['content']);

        //now that the email has been sent, we need to switch the notified flag on their account
        $employee->email_sent = 1;
        $employee->save();    
    }
    catch (exception $e) {
        die("<h2>Sorry: " . $e->getMessage() . "</h2>" );
    }

    //we've' successfuly added the employee, audit log entry and notified them: so we will consider this user logged in
    $_SESSION["logged_in_user_id"] = $employee->id;

    header('Location: dashboard.php');
}

//fix me: this needs to be moved out into a view later
if (empty($_POST)): ?>

<h1>Create New Employee</h1>
<form action="" method="post">
    <ul>
        <li>
            Employee Name:
            <input name="name" type="text" />
        </li>
        <li>
            Phone Number:
            <input name="phone_number" type="text"/>
        </li>
        <li>
            Password:
            <input name="password" type="password" />
        </li>
        <li>
            Email:
            <input name="email" type="text"/>
        </li>
        <li>
            Employee Type:
            <select name="employee_type">
                <option value="<?php echo \models\Employee::TYPE_PART_TIME; ?>">Part Time</option>
                <option value="<?php echo \models\Employee::TYPE_FULL_TIME; ?>">Full Time</option>
            </select>
        </li>
        <li>
            Employee Gender:
            <select name="gender">
                <option value="<?php echo \models\Employee::TYPE_GENDER_MALE; ?>">Male</option>
                <option value="<?php echo \models\Employee::TYPE_GENDER_FEMALE; ?>">Female</option>
            </select>
        </li>
    </ul>
    <input type="submit" value="Create">
</form>
<?php endif; ?>