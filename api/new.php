<?php

require_once("../models/AuditLog.php");
require_once("../models/Employee.php");

/**
 * For the purposes of the test, the way that the data comes in to the system is not relevant. Let's assume that
 * we have a REST API, and this is a POST request to the "new employee" endpoint. The data provided is the array
 * that has been specified here.
 */

//allowed data is "name", "number", "email", "type" & "gender"
$receivedData = array(
    "name"   => "Bob Smith",
    "email"  => "luke.zawadzki@astutepayroll.com",
    "number" => "+61 430 131 409",
    "type"   => "full-time",
    "gender" => "male"
);

//this simulates a call to the API.
//fixme: naming not ideal and also within global namespace
var_dump(handle_API_Request($receivedData));

/**
 * This is the processing code for the API.
 */
function handle_API_Request($data) {
    try {
        /**
         * technical debt: this sequence is very similar to 
         * /web_app/new.php and needs to be moved into a reusable 
         * service/domain layer
         */
        $employee              = new \models\Employee();
        $employee->name        = $data['name'];
        $employee->email       = $data['email'];
        $employee->phoneNumber = $data['number'];
        $employee->type        = $data['type'];
        $employee->gender      = $data['gender'];
        $generatedPassword     = uniqid();
        $employee->password    = sha1($generatedPassword);

        $employee->save();

        $audit_log          = new \models\AuditLog();
        $timestamp          = date("d/m/y h:i:s");
        $audit_log->message = sprintf("%s was added on %s", $_POST['name'], $timestamp);
        $audit_log->save();

        //notify the new employee via email
        //technical debt: do we really want plaintext password in this email?
        $email = array(
            'recipient' => $employee->email,
            'subject'   => "Thanks for registering",
            'content'   => sprintf("Dear %s,\nYou have been added to AwesomeCorp! your password is %s.\nYou can login at: %s.\nRegards,\nRecruiterForce",
                                $employee->name, 
                                $generatedPassword, 
                                "http://awesomecorp.recruiterforce.com/login"
                            )
        );

        //technical debt: we should move this to something more accessible like mailchimp
        //technical debt: mailing and updating the flag need to be combined  
        mail($email['recipient'], $email['subject'], $email['content']);

        //now that the email has been sent, we need to switch the notified flag on their account
        $employee->email_sent = 1;
        $employee->save();    

        //technical debt: better status codes available, but leaving to maintain integrations
        return array(
            "status"  => "success",
            "message" => sprintf("%s was added", $employee->id)
        );
    }
    catch (exception $e) {
        //technical debt: better status codes available, but leaving to maintain integrations
        return array(
            "status"  => "error",
            "message" => $e->getMessage()
        );
    }

}