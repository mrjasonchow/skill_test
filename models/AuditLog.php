<?php

namespace models;

/**
 * Class AuditLog
 *
 * This model represents an audit log operation in our system.
 */
class AuditLog 
{

    /**
     * The unique identifier
     * @var int
     */
    public $id;

    /**
     * The operation executed
     * @var string
     */
    public $message;

    /**
     * Save method (this has been hacked to save to the database, but it should be presumed that this would happen in
     * an ORM of some sort - and that the ORM takes care of the DB connection, query, etc.)
     *
     * It should also be assumed that the save method will work nicely for inserting and saving updates to an already existing employee.
     *
     * @return void
     * @throws Exception if we had a problem saving.
     */
    public function save() 
    {
        //technical debt: move this into a basemodel to avoid repetition
        $pdo = new \PDO("mysql:host=localhost;dbname=test", "root");

        //we only support inserts
        $stmt = $pdo->prepare("INSERT INTO audit_log (`message`) VALUES (:message)");
        $stmt->bindParam(":message", $this->message);

        if (!$stmt->execute()) 
        {
            throw new Exception("Could not save audit log.");
        }

        //make sure we make the newly created identifier available
        $this->id = $pdo->lastInsertId();
    }

}